//
// Created by jonathan.hanks on 2/1/21.
//

#ifndef LIBCDS_PUBSUB_PIPELINE_PARSER_HH
#define LIBCDS_PUBSUB_PIPELINE_PARSER_HH

#include <string>
#include <vector>

namespace pub_sub
{
    namespace detail
    {

        std::vector< std::string > parse_pipeline( const std::string& input );

    }
} // namespace pub_sub

#endif // LIBCDS_PUBSUB_PIPELINE_PARSER_HH
