#!/bin/bash

# this script is used to call dch forcing the version to be what is in
# the version.txt and version_tag.txt files
VER=`cat version.txt | grep -v \# | head -1`
TAG=`cat version_tag.txt | grep -v \# | head -1`

dch -b -v "${VER}${TAG}" -D UNRELEASED 'CI Build for testing only'