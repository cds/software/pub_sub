#!/bin/bash

if which clang-format-7 > /dev/null; then
    CFORMAT=`which clang-format-7`
elif which clang-format > /dev/null; then
    CFORMAT=`which clang-format`
else
    echo "Could not find clang format"
    exit 1
fi

SOURCE_DIRS="pub_sub examples python"

for dir in ${SOURCE_DIRS}; do
    echo "Reformatting in ${dir}"
    find ${dir} -type f -iregex ".*\.\(c\|cc\|cxx\|cpp\|h\|hh\|hpp\|hxx|hh.in|cc.in\)" \! -iname catch.hpp \! -exec "${CFORMAT}" -i -style=file -fallback-style=none {} \;
done
