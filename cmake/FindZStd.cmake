
if (cds_find_zstd_included)
    return()
endif(cds_find_zstd_included)
set(cds_find_zstd_included TRUE)

find_package(PkgConfig)

pkg_check_modules(zstd REQUIRED IMPORTED_TARGET GLOBAL libzstd)

add_library(zstd::zstd ALIAS PkgConfig::zstd)
