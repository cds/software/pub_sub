#include <atomic>
#include <csignal>
#include <iostream>
#include <thread>
#include <random>

#include <cds-pubsub/sub.hh>

std::atomic< bool > done{ false };

void
signal_handler( int signal_number )
{
    done = true;
}

int
main( int argc, char* argv[] )
{
    std::random_device  rd;
    std::mt19937        gen( rd( ) );
    pub_sub::Subscriber sub;

    int message_burst_size = 10;

    std::signal( SIGINT, signal_handler );
    sub.subscribe( "tcp://127.0.0.1:9000/",
                   [ &message_burst_size, &gen ]( pub_sub::SubMessage msg ) {
                       std::cout << "Key = " << msg.key( )
                                 << " size = " << msg.size( ) << std::endl;
                       --message_burst_size;
                       if ( message_burst_size < 0 )
                       {
                           std::uniform_int_distribution< int > dist( 0, 10 );
                           std::this_thread::sleep_for(
                               std::chrono::seconds( dist( gen ) ) );
                           message_burst_size = 100;
                       }
                   } );
    while ( !done )
    {
        std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
    }
    return 0;
}