/* pub_sub publisher implementation
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <cds-pubsub/version.hh>
#include "pub_sub_intl.hh"

namespace pub_sub
{
    namespace detail
    {
        SubDebugNotices*
        NullSubDebugNotices( )
        {
            static SubDebugNotices null_debug{ };
            return &null_debug;
        }

        PubDebugNotices*
        NullPubDebugNotices( )
        {
            static PubDebugNotices null_debug{ };
            return &null_debug;
        }

    } // namespace detail

    std::string
    version( )
    {
        return CDS_PUB_SUB_VERSION;
    }

} // namespace pub_sub